﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SteamFarm.Mysql;
using SteamFarm.Server;

namespace SteamFarm.Utils {
   public class Config : IDisposable {

        private static Config instance;
        public static Config i() {
            if (instance == null) instance = new Config();
            return instance;
        }

        public Guid Guid { get; set; }
        public uint CellID { get; set; }

        public readonly InMemoryServerListProvider ServerListProvider = new InMemoryServerListProvider();

        public void Load(Table.Config Table) {
            this.CellID = Table.CellID;
        }

        private Config() => ServerListProvider.ServerListUpdated += OnServerListUpdated;
        private async void OnServerListUpdated(object sender, EventArgs e) => await Save().ConfigureAwait(false);

        public async Task Save() {
            using (var a = new Database()) {
                var T = new Table.Config {
                    Id = 1,
                    CellID = Config.i().CellID
                };
                a.Update(T);
                await a.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public void Dispose() {
            ServerListProvider.ServerListUpdated -= OnServerListUpdated;
        }
    }
}
