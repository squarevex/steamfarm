﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SteamFarm {
    public class Value {

        public static bool IsNullOrEmpty(string value, bool ShowMsg = true) {
            if(string.IsNullOrEmpty(value)) {
                if (ShowMsg) Program.Log.NullError(nameof(value));
                return true;
            }
            return false;
        }
        public static bool IsNull(object value, bool ShowMsg = true) {
            if (value == null) {
                if (ShowMsg) Program.Log.NullError(nameof(value));
                return true;
            }
            return false;
        }
    }
}
