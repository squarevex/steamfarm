﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace SteamFarm {
    internal static class CryptoHelper {
        private static byte[] EncryptionKey = Encoding.UTF8.GetBytes(SharedInfo.AssemblyName);

        internal static string Decrypt(ECryptoMethod cryptoMethod, string encrypted) {
            if(Value.IsNullOrEmpty(encrypted)) return null;

            switch (cryptoMethod) {
                case ECryptoMethod.PlainText:
                    return encrypted;
                case ECryptoMethod.AES:
                    return DecryptAES(encrypted);
                default:
                    return null;
            }
        }

        internal static string Encrypt(ECryptoMethod cryptoMethod, string decrypted) {
            if (Value.IsNullOrEmpty(decrypted)) return null;

            switch (cryptoMethod) {
                case ECryptoMethod.PlainText:
                    return decrypted;
                case ECryptoMethod.AES:
                    return EncryptAES(decrypted);
                default:
                    return null;
            }
        }

        internal static void SetEncryptionKey(string key) {
            if (Value.IsNullOrEmpty(key)) return;
            EncryptionKey = Encoding.UTF8.GetBytes(key);
        }

        private static string DecryptAES(string encrypted) {
            if (Value.IsNullOrEmpty(encrypted)) return null;

            try {
                byte[] key;
                using (SHA256 sha256 = SHA256.Create()) {
                    key = sha256.ComputeHash(EncryptionKey);
                }

                byte[] decryptedData = Convert.FromBase64String(encrypted);
                decryptedData = SteamKit2.CryptoHelper.SymmetricDecrypt(decryptedData, key);
                return Encoding.UTF8.GetString(decryptedData);
            } catch (Exception e) {
                Program.Log.Exception(e);
                return null;
            }
        }

        private static string EncryptAES(string decrypted) {
            if (Value.IsNullOrEmpty(decrypted)) return null;

            try {
                byte[] key;
                using (SHA256 sha256 = SHA256.Create()) {
                    key = sha256.ComputeHash(EncryptionKey);
                }

                byte[] encryptedData = Encoding.UTF8.GetBytes(decrypted);
                encryptedData = SteamKit2.CryptoHelper.SymmetricEncrypt(encryptedData, key);
                return Convert.ToBase64String(encryptedData);
            } catch (Exception e) {
                Program.Log.Exception(e);
                return null;
            }
        }

        internal enum ECryptoMethod : byte {
            PlainText,
            AES
        }
    }
}
