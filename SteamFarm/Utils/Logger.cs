﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Text;

namespace SteamFarm {
    public class Logger {
        private string Path;
        private string Name;

        public Logger(string Path, string Name) {
            this.Path = SharedInfo.LoggerPath + Path + ".log";
            this.Name = Name;
        }

        private void Write(string Message) {
            Console.WriteLine($"{Message}");
            using (StreamWriter SW = new StreamWriter(this.Path, true)) {
                SW.WriteLine(Message);
            }
        }

        public void Info(string Message, [CallerMemberName] string PreviousMethodName = null) {
            Write($"[INFO][{Name}] >" + $" {Message}");
        }

        public void Warning(string Message, [CallerMemberName] string PreviousMethodName = null) {
            Write($"[WARN][{Name}] >" + $" {Message}");
        }

        public void Error(string Message, [CallerMemberName] string PreviousMethodName = null) {
            Write($"[ERROR][{Name}] >" + $" {Message}");
        }

        public void Exception(Exception Exception, [CallerMemberName] string PreviousMethodName = null) {
            Write($"[EXEC][{Name}] >" + $"{PreviousMethodName}() {Exception.StackTrace}");
        }

        public void NullError(string NullObjectName, [CallerMemberName] string PreviousMethodName = null) {
            Error(string.Format(Strings.IsNull, NullObjectName), PreviousMethodName);
        }

        public void FatalException(Exception Exception, [CallerMemberName] string PreviousMethodName = null) {
            string Message = string.Format("Fatal Exception: " + Exception.Message + Environment.NewLine + Exception.StackTrace);
            Write(Message);
        }
    }
}
