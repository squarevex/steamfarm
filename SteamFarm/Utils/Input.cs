﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SteamFarm {
    public class Input {

        private static readonly object ConsoleLock = new object();

        public static string GetUserInput(EUserInputType userInputType, string Username) {
            if (userInputType == EUserInputType.Unknown) return null;

            string result = null;
            lock (ConsoleLock) {
                try {
                    switch (userInputType) {
                        case EUserInputType.DeviceID:
                            Console.Write("[INFO]["+Username+"] > " + Strings.UserInputDeviceID);
                            result = Console.ReadLine();
                            break;
                        case EUserInputType.TwoFactorAuthentication:
                            Console.Write("[INFO][" + Username + "] > " + Strings.UserInputSteam2FA);
                            result = Console.ReadLine();
                            break;
                        case EUserInputType.SteamGuard:
                            Console.Write("[INFO][" + Username + "] > " + Strings.UserInputSteamGuard);
                            result = Console.ReadLine();
                            break;
                        default:
                            Program.Log.Info(string.Format(Strings.UserInputUnknownValue, nameof(userInputType), userInputType));
                            Console.Write("[INFO][" + Username + "] > " + Strings.UserInputSteamGuard);
                            result = Console.ReadLine();
                            break;
                    }
                    if (!Console.IsOutputRedirected) Console.Clear();
                } catch (Exception e) {
                    Program.Log.Exception(e);
                    return null;
                }
            }
            return !string.IsNullOrEmpty(result) ? result.Trim() : null;
        }




        public enum EUserInputType : byte {
            Unknown,
            DeviceID,
            Login,
            Password,
            SteamGuard,
            SteamParentalPIN,
            TwoFactorAuthentication
        }
    }
}
