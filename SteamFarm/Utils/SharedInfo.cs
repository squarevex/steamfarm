﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace SteamFarm {
    public static class SharedInfo {

        public const string AssemblyName = nameof(SteamFarm);
        public static Guid ModuleVersion => Assembly.GetEntryAssembly().ManifestModule.ModuleVersionId;
        public static Version Version => Assembly.GetEntryAssembly().GetName().Version;

        public static string RootPath   = "";
        public static string LoggerPath = "";
        public static string SentryPath = "";
        public static string AuthPath   = "";

        public static byte ConnectionTimeout = 60;

        public static byte MaxConnections = 10; //Max connections per ServicePoint
        public const byte MaxTries = 5; //Max tries for a single request
        public static byte MaxIdleTime = 15; //Seconds
        public static byte MaxTwoFactorCodeFailures = 3;

        public static byte MinPlayingBlockedTTL = 60;
        public static byte MinHeartBeatTTL = ConnectionTimeout;

        public static ushort WebLimiterDelay = 600; //Min value 200

        public static byte ExtendedTimeoutMultiplier = 10; // Defines multiplier of timeout for WebBrowsers dealing with huge data
        public static ushort CallbackSleep = 500; //Miliseconds
        

        public static char[] CodeCharacters = { '2', '3', '4', '5', '6', '7', '8', '9', 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'T', 'V', 'W', 'X', 'Y' };
        public static byte CodeDigits = 5;
        public static byte CodeInterval = 30;
        public static byte SteamTimeTTL = 24; // For how many hours we can assume that SteamTimeDifference is correct

        public static byte LoginCooldownInMinutes = 25; // Captcha disappears after around 20 minutes, so we make it 25
        public static byte LoginLimiterDelay = 10;

        // Checks if the program is running in a IDE or not
        public static bool IsDebug() {
            return Debugger.IsAttached;
        }
    }
}
