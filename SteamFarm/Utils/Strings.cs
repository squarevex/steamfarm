﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SteamFarm {
    class Strings {

        public static string IsNull = "{0} is null!";
        public static string Initializing = "Initializing {0}...";
        public static string InitializingSuccess = "Initializing Success!"; 

        public static string BotSteamDirectoryInitializationFailed = "Could not initialize SteamDirectory: connecting with Steam Network might take much longer than usual!";
        public static string BotAlreadyRegistrered = "{0} is already registrered";
        public static string BotDisabled = "Wont start this bot because it's disabled.";
        public static string BotNoLogin = "Username or Password is empty";
        public static string BotStarting = "Starting...";
        public static string BotStopping = "Stopping!";
        public static string BotHeartBeatFailed = "Failed to disconnect the client.";
        public static string BotRemovedExpiredLoginKey = "Removed expired login key!";
        public static string BotRateLimitExceeded = "Rate limit exceeded, we will retry after {0} of cooldown...";
        public static string BotInvalidAuthenticatorDuringLogin = "Received TwoFactorCodeMismatch error code {0} times in a row, this almost always indicates invalid 2FA credentials, aborting!";

        public static string BotAuthenticatorConverting      = "Converting .maFile...";
        public static string BotAuthenticatorInvalidDeviceID = "Your DeviceID is incorrect or doesn't exist!";
        public static string BotAuthenticatorImportFinished  = "Successfully finished importing mobile authenticator!";

        public static string BotReconnecting   = "Reconnecting...";
        public static string BotConnectionLost = "Connection to Steam Network lost.Reconnecting...";

        public static string BotLogonSessionReplaced = "This account seems to be used in another instance, which is undefined behaviour, refusing to keep it running!";
        public static string BotUnableToLogin = "Unable to login to Steam: {0}/{1}";

        public static string BotAccountLimited = "This account is limited, idling process is unavailable until the restriction is removed!";
        public static string BotAccountLocked = "This account is locked, idling process is permanently unavailable!";

        public static string BotConnecting = "Connecting...";
        public static string BotConnected = "Connected!";
        public static string BotLoggingIn = "Logging in...";
        public static string BotLoggedOn  = "Successfully logged on!";
        public static string BotLoggedOff = "Logged off of Steam: {0}";
        public static string BotDisconnecting = "Disconnecting...";
        public static string BotDisconnected = "Disconnected!";

        public static string NoBotsRunning = "No bots are running!";

        public static string WebLogin = "Logging in to {0}...";
        public static string WebLoginSuccess = "Login success!";
        public static string WebRefreshSession = "Refreshing our session!";

        public static string ErrorZeroCode = "Exiting with nonzero error code!";
        public static string ErrorInvalid = "{0} is invalid!";

        public static string WarningUnknownValuePleaseReport = "Received unknown value for {0}, please report this: {1}";
        public static string ErrorRequestFailedTooManyTimes = "Request failed after {0} attempts!";

        public static string UserInputDeviceID     = "Please enter your mobile authenticator device ID (including 'android:'):";
        public static string UserInputSteam2FA     = "Please enter your 2FA code from your Steam authenticator app:";
        public static string UserInputSteamGuard   = "Please enter SteamGuard auth code that was sent on your e-mail:";
        public static string UserInputUnknown      = "Please enter undocumented value of {0}:";
        public static string UserInputUnknownValue = "Received unknown value for {0}";
    }
}
