﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Humanizer;
using Humanizer.Localisation;

namespace SteamFarm {
    public static class Utilities {

        public static void InBackground(Action action, bool longRunning = false) {
            if (Value.IsNull(action)) return;

            TaskCreationOptions options = TaskCreationOptions.DenyChildAttach;
            if (longRunning) options |= TaskCreationOptions.LongRunning | TaskCreationOptions.PreferFairness;
            Task.Factory.StartNew(action, CancellationToken.None, options, TaskScheduler.Default);
        }
        public static void InBackground<T>(Func<T> function, bool longRunning = false) {
            if (Value.IsNull(function)) return;

            TaskCreationOptions options = TaskCreationOptions.DenyChildAttach;
            if (longRunning) options |= TaskCreationOptions.LongRunning | TaskCreationOptions.PreferFairness;
            Task.Factory.StartNew(function, CancellationToken.None, options, TaskScheduler.Default);
        }

        public static uint GetUnixTime() => (uint)DateTimeOffset.UtcNow.ToUnixTimeSeconds();

        public static bool IsRunningOnMono => Type.GetType("Mono.Runtime") != null;

        public static string GetCookieValue(this CookieContainer cookieContainer, string url, string name) {
            if ((cookieContainer == null) || string.IsNullOrEmpty(url) || string.IsNullOrEmpty(name)) {
                Program.Log.NullError(nameof(cookieContainer) + " || " + nameof(url) + " || " + nameof(name));
                return null;
            }

            Uri uri;

            try {
                uri = new Uri(url);
            } catch (UriFormatException e) {
                Program.Log.Exception(e);
                return null;
            }

            CookieCollection cookies = cookieContainer.GetCookies(uri);
            return cookies.Count > 0 ? (from Cookie cookie in cookies where cookie.Name.Equals(name) select cookie.Value).FirstOrDefault() : null;
        }

        public static bool IsValidHexadecimalString(string text) {
            if (string.IsNullOrEmpty(text)) {
                Program.Log.NullError(nameof(text));
                return false;
            }
            const byte split = 16;
            for (byte i = 0; i < text.Length; i += split) {
                string textPart = string.Join("", text.Skip(i).Take(split));
                if (!ulong.TryParse(textPart, NumberStyles.HexNumber, null, out _)) return false;
            }
            return true;
        }

    }
}
