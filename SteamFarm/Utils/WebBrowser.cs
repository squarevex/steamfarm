﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SteamFarm {
    public class WebBrowser : IDisposable {

        private Logger Log;

        private readonly HttpClient HttpClient;
        public readonly CookieContainer CookieContainer = new CookieContainer();

        public TimeSpan Timeout => HttpClient.Timeout;

        public WebBrowser(Logger Log, bool extendedTimeout = false) {
            this.Log = Log;

            HttpClientHandler httpClientHandler = new HttpClientHandler {
                AllowAutoRedirect = false, // This must be false if we want to handle custom redirection schemes such as "steammobile"
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip,
                CookieContainer = CookieContainer,
                Proxy = null,
                UseProxy = false
            };

            if (!Utilities.IsRunningOnMono) httpClientHandler.MaxConnectionsPerServer = SharedInfo.MaxConnections;
            HttpClient = new HttpClient(httpClientHandler) { Timeout = TimeSpan.FromSeconds(extendedTimeout ? SharedInfo.ExtendedTimeoutMultiplier * SharedInfo.ConnectionTimeout : SharedInfo.ConnectionTimeout) };
            
            // Most web services expect that UserAgent is set, so we declare it globally
            HttpClient.DefaultRequestHeaders.UserAgent.ParseAdd(SharedInfo.AssemblyName + "/" + SharedInfo.Version);

        }

        public void Dispose() => HttpClient.Dispose();

        public static void Init() {
            // Set max connection limit from default of 2 to desired value
            ServicePointManager.DefaultConnectionLimit = SharedInfo.MaxConnections;

            // Set max idle time from default of 100 seconds (100 * 1000) to desired value
            ServicePointManager.MaxServicePointIdleTime = SharedInfo.MaxIdleTime * 1000;

            // Don't use Expect100Continue, we're sure about our POSTs, save some TCP packets
            ServicePointManager.Expect100Continue = false;

            // Reuse ports if possible
            if (!Utilities.IsRunningOnMono) ServicePointManager.ReusePort = true;
        }

        private static HtmlDocument StringToHtmlDocument(string html) {
            if (html == null) {
                Program.Log.NullError(nameof(html));
                return null;
            }

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);

            return htmlDocument;
        }
        /* ---------------------------------------------------------------------*/
        /* URL GET - POST
        /* -------------------------------------------------------------------- */
        public async Task<BasicResponse> UrlPost(string request, IReadOnlyCollection<KeyValuePair<string, string>> data = null, string referer = null, byte maxTries = SharedInfo.MaxTries) {
            if (string.IsNullOrEmpty(request) || (maxTries == 0)) {
                Log.NullError(nameof(request) + " || " + nameof(maxTries));
                return null;
            }

            for (byte i = 0; i < maxTries; i++) {
                using (HttpResponseMessage response = await InternalPost(request, data, referer).ConfigureAwait(false)) {
                    if (response == null) continue;
                    return new BasicResponse(response);
                }
            }

            if (maxTries > 1) Log.Warning(string.Format(Strings.ErrorRequestFailedTooManyTimes, maxTries));
            return null;
        }
        public async Task<HtmlDocumentResponse> UrlGetToHtmlDocument(string request, string referer = null, byte maxTries = SharedInfo.MaxTries) {
            if (string.IsNullOrEmpty(request) || (maxTries == 0)) {
                Log.NullError(nameof(request) + " || " + nameof(maxTries));
                return null;
            }

            StringResponse response = await UrlGetToString(request, referer, maxTries).ConfigureAwait(false);
            return response != null ? new HtmlDocumentResponse(response) : null;
        }

        private async Task<StringResponse> UrlGetToString(string request, string referer = null, byte maxTries = SharedInfo.MaxTries) {
            if (string.IsNullOrEmpty(request) || (maxTries == 0)) {
                Log.NullError(nameof(request) + " || " + nameof(maxTries));
                return null;
            }

            for (byte i = 0; i < maxTries; i++) {
                using (HttpResponseMessage response = await InternalGet(request, referer).ConfigureAwait(false)) {
                    if (response == null) continue;
                    return new StringResponse(response, await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                }
            }
            if (maxTries > 1) Log.Warning(string.Format(Strings.ErrorRequestFailedTooManyTimes, maxTries));
            return null;
        }
        /* ---------------------------------------------------------------------*/
        /* Internal POST - GET- REQUEST
        /* -------------------------------------------------------------------- */
        private async Task<HttpResponseMessage> InternalPost(string request, IReadOnlyCollection<KeyValuePair<string, string>> data = null, string referer = null) {
            if (string.IsNullOrEmpty(request)) {
                Log.NullError(nameof(request));
                return null;
            }
            return await InternalRequest(new Uri(request), HttpMethod.Post, data, referer).ConfigureAwait(false);
        }

        private async Task<HttpResponseMessage> InternalGet(string request, string referer = null, HttpCompletionOption httpCompletionOptions = HttpCompletionOption.ResponseContentRead) {
            if (string.IsNullOrEmpty(request)) {
                Log.NullError(nameof(request));
                return null;
            }
            return await InternalRequest(new Uri(request), HttpMethod.Get, null, referer, httpCompletionOptions).ConfigureAwait(false);
        }

        private async Task<HttpResponseMessage> InternalRequest(Uri requestUri, HttpMethod httpMethod, IReadOnlyCollection<KeyValuePair<string, string>> data = null, string referer = null, HttpCompletionOption httpCompletionOption = HttpCompletionOption.ResponseContentRead, byte maxRedirections = SharedInfo.MaxTries) {
            if ((requestUri == null) || (httpMethod == null)) {
                Log.NullError(nameof(requestUri) + " || " + nameof(httpMethod));
                return null;
            }

            HttpResponseMessage response;

            using (HttpRequestMessage request = new HttpRequestMessage(httpMethod, requestUri)) {
                if (data != null) {
                    try {
                        request.Content = new FormUrlEncodedContent(data);
                    } catch (UriFormatException e) {
                        Log.Exception(e);
                        return null;
                    }
                }
                if (!string.IsNullOrEmpty(referer)) request.Headers.Referrer = new Uri(referer);
                try {
                    response = await HttpClient.SendAsync(request, httpCompletionOption).ConfigureAwait(false);
                } catch (Exception e) {
                    Log.Exception(e);
                    return null;
                }
            }
            if (response == null) return null;
            if (response.IsSuccessStatusCode) return response;

            // WARNING: We still have undisposed response by now, make sure to dispose it ASAP if we're not returning it!
            if ((response.StatusCode >= HttpStatusCode.Ambiguous) && (response.StatusCode < HttpStatusCode.BadRequest) && (maxRedirections > 0)) {
                Uri redirectUri = response.Headers.Location;

                if (redirectUri.IsAbsoluteUri) {
                    switch (redirectUri.Scheme) {
                        case "http":
                        case "https":
                            break;
                        case "steammobile":
                            // Those redirections are invalid, but we're aware of that and we have extra logic for them
                            return response;
                        default:
                            // We have no clue about those, but maybe HttpClient can handle them for us
                            Log.Error(string.Format(Strings.WarningUnknownValuePleaseReport, nameof(redirectUri.Scheme), redirectUri.Scheme));
                            break;
                    }
                } else {
                    redirectUri = new Uri(requestUri.GetLeftPart(UriPartial.Authority) + redirectUri);
                }
                response.Dispose();
                return await InternalRequest(redirectUri, httpMethod, data, referer, httpCompletionOption, --maxRedirections).ConfigureAwait(false);
            }

            using (response) return null;
        }

        public class HtmlDocumentResponse : BasicResponse {
            public readonly HtmlDocument Content;

            public HtmlDocumentResponse(StringResponse stringResponse) : base(stringResponse) {
                if (stringResponse == null) {
                    throw new ArgumentNullException(nameof(stringResponse));
                }

                Content = StringToHtmlDocument(stringResponse.Content);
            }
        }

        public class StringResponse : BasicResponse {
            public readonly string Content;

            public StringResponse(HttpResponseMessage httpResponseMessage, string content) : base(httpResponseMessage) {
                if ((httpResponseMessage == null) || (content == null)) {
                    throw new ArgumentNullException(nameof(httpResponseMessage) + " || " + nameof(content));
                }

                Content = content;
            }
        }

        public class BasicResponse {
            public readonly Uri FinalUri;

            public BasicResponse(HttpResponseMessage httpResponseMessage) {
                if (httpResponseMessage == null) throw new ArgumentNullException(nameof(httpResponseMessage));
                FinalUri = httpResponseMessage.Headers.Location ?? httpResponseMessage.RequestMessage.RequestUri;
            }

            public BasicResponse(BasicResponse basicResponse) {
                if (basicResponse == null) throw new ArgumentNullException(nameof(basicResponse));
                FinalUri = basicResponse.FinalUri;
            }
        }
    }
}
