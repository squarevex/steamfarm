﻿using SteamFarm.Mysql;
using SteamFarm.Utils;
using SteamKit2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamFarm {
    public static class Index {

        public static async Task InitBots() {
            if (Bot.Bots.Count != 0) return;

            await Bot.InitializeSteamConfiguration(ProtocolTypes.Tcp | ProtocolTypes.Udp, Config.i().CellID, Config.i().ServerListProvider).ConfigureAwait(false);

            //Get bots from mysql
            using (var a = new Database()) {
                var Bots = a.GetBots();

                //Register all bots and send all settings
                foreach (var x in Bots) {
                    await Bot.Register(x).ConfigureAwait(false);
                }
            }
        }
    }
}
