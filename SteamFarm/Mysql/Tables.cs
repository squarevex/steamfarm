﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SteamFarm.Table {
    public class Bot {
        public int    Id { get; set; }
        public bool   Enabled { get; set; }
        public bool   IsBot { get; set; }
        public bool   Farm_Offline { get; set; }
        public string Displayname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Trade_Token { get; set; }
        public bool   Dismiss_Inventory_Notification { get; set; }
        public string LoginKey { get; set; }
        public bool   Has_Mobile_Authenticator { get; set; }
        public string Auth_DeviceID { get; set; }
        public string Auth_SharedSecret { get; set; }
        public string Auth_IdentitySecret { get; set; }
    }

    public class Config {
        public int   Id { get; set; } //Ignore me! we need a primary key
        public uint CellID { get; set; }

    }
}
