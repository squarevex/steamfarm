﻿using Microsoft.EntityFrameworkCore;
using SteamFarm.Table;
using System.Collections.Generic;
using System.Linq;

namespace SteamFarm.Mysql {
    public class Database : DbContext {

        public DbSet<Table.Bot> Steam_Bots { get; set; }
        public DbSet<Table.Config> Steam_Config { get; set; }

        public Database() {
            //Supposed to create Database if it does not exists
            //Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder ob) {
            ob.UseMySql(@"Server=localhost;database=cweye;uid=root;pwd=root;");
        }
        
        public List<Table.Bot> GetBots() {
            var Bots = Steam_Bots.Select(x => new Table.Bot {
                Id = x.Id,
                Enabled = x.Enabled,
                IsBot = x.IsBot,
                Farm_Offline = x.Farm_Offline,
                Displayname = x.Displayname,
                Username = x.Username,
                Password = x.Password,
                Trade_Token = x.Trade_Token,
                Dismiss_Inventory_Notification = x.Dismiss_Inventory_Notification,
            }).ToList();

            return Bots;
        }
        /*
        https://github.com/yukozh/YuukoBlog-NETCore-MySql/blob/master/src/YuukoBlog/Controllers/BaseController.cs

        //Insert
        using (var Db = new Db()) {
            Db.Database.EnsureCreated();
            var user = new Bot { id = 5, name = "Yuuko" };
            Db.Add(user); //Db.Bots.Add(user);
            Db.SaveChanges();
        }
        
        //Select
        var bots = Db.Bots.Where(x => x.id.Equals(1)).Select....
        var bots = Db.Bots.Select(x => new Bot {
            id = x.id,
            name = x.name
        }).ToList();

        foreach (var x in bots) {
            Console.WriteLine(x.id + " " + x.name);
        }
         
        //Update
        using (var a = new ConfigModel()) {
            var v = new Table.Config {
                Id = 1,
                CellID = 2
            };
            a.Update(v);
            a.SaveChanges();
            Logger.Info("Config Updated!");
        }
         
         
         
         
         
         
         
         
         */




    }
}
