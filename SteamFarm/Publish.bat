@echo off

REM Delete the old folder
@RD /S /Q "%cd%\bin\publish"

REM Publish the project
dotnet publish

REM Move the publish folder to bin
move "%cd%\bin\Debug\netcoreapp2.0\publish" "%cd%\bin\"

REM Open the published folder
%SystemRoot%\explorer.exe "%cd%\bin\publish"

REM Create Launch bat
>"%cd%\bin\publish\"Run.bat (
echo @echo off
echo dotnet SteamFarm.dll
echo pause
)


