﻿using MySql.Data.MySqlClient;
using SteamFarm.Mysql;
using SteamFarm.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Configuration;

namespace SteamFarm {
    internal static class Program {

        private static readonly TaskCompletionSource<byte> ShutdownResetEvent = new TaskCompletionSource<byte>();
        private static bool ShutdownSequenceInitialized;

        public static Logger Log;

        private static void Main(string[] args) {
            Task.Run(async () => {
                // Initialize
                InitDirectory();
                Log = new Logger("Program", "SYS");
                AppDomain.CurrentDomain.ProcessExit += OnProcessExit;
                AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
                TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;

                await Init(args).ConfigureAwait(false);
                //Wait for shutdown event
                return await ShutdownResetEvent.Task.ConfigureAwait(false);
            }).GetAwaiter().GetResult();
        }

        private static async Task Init(IReadOnlyCollection<string> args) {
            Log.Info(SharedInfo.AssemblyName + " V" + SharedInfo.Version + " (" + SharedInfo.ModuleVersion + ")");

            WebBrowser.Init();

            using (var a = new Database()) {
                var c = a.Steam_Config.Select(x => new Table.Config {
                    Id = x.Id,
                    CellID = 0,

                }).SingleOrDefault();
                Config.i().Load(c);
                Config.i().Guid = Guid.NewGuid();
                Log.Info("Config Loaded!");
            }
            await Index.InitBots().ConfigureAwait(false);
        }

        private static void InitDirectory() {
            SharedInfo.RootPath = AppDomain.CurrentDomain.BaseDirectory;
            if (SharedInfo.IsDebug()) SharedInfo.RootPath = Path.GetFullPath(Path.Combine(SharedInfo.RootPath, @"..\..\..\Debug\"));
            
            SharedInfo.LoggerPath = Path.Combine(SharedInfo.RootPath, "Log\\");
            SharedInfo.SentryPath = Path.Combine(SharedInfo.RootPath, "Bin\\");
            SharedInfo.AuthPath   = Path.Combine(SharedInfo.RootPath, "Auth\\");

            Directory.CreateDirectory(SharedInfo.LoggerPath);
            Directory.CreateDirectory(SharedInfo.SentryPath);
            Directory.CreateDirectory(SharedInfo.AuthPath);
        }

        private static async void OnProcessExit(object sender, EventArgs e) => await Shutdown().ConfigureAwait(false);
        private static async void OnUnhandledException(object sender, UnhandledExceptionEventArgs e) {
            if (e?.ExceptionObject == null) {
                Log.NullError(nameof(e) + " || " + nameof(e.ExceptionObject));
                return;
            }
            Log.FatalException((Exception)e.ExceptionObject);
            await Exit(1).ConfigureAwait(false);
        }
        private static void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e) {
            if (e?.Exception == null) {
                Log.NullError(nameof(e) + " || " + nameof(e.Exception));
                return;
            }
            Log.FatalException(e.Exception);
            e.SetObserved();
        }

        internal static async Task Exit(byte exitCode = 0) {
            if (exitCode != 0) Log.Error(Strings.ErrorZeroCode);
            await Shutdown(exitCode).ConfigureAwait(false);
            Environment.Exit(exitCode);
        }

        private static async Task Shutdown(byte exitCode = 0) {
            if (!await InitShutdownSequence().ConfigureAwait(false)) return;
            ShutdownResetEvent.TrySetResult(exitCode);
        }

        private static async Task<bool> InitShutdownSequence() {
            if (ShutdownSequenceInitialized) return false;
            ShutdownSequenceInitialized = true;

            if (Bot.Bots.Count > 0) {
                IEnumerable<Task> Tasks = Bot.Bots.Values.Select(Bot => Task.Run(() => Bot.Stop(true)));
                await Task.WhenAny(Task.WhenAll(Tasks), Task.Delay(Bot.Bots.Count * SharedInfo.MaxTries * 1000)).ConfigureAwait(false);
                // Extra second for Steam requests to go through
                await Task.Delay(1000).ConfigureAwait(false);
            }
            return true;
        }
    }
}
