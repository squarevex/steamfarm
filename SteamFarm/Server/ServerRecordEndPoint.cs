﻿using SteamKit2;
using System;
using System.Collections.Generic;
using System.Text;

namespace SteamFarm.Server {
    public class ServerRecordEndPoint {

        internal readonly string Host;
        internal readonly ushort Port;
        internal readonly ProtocolTypes ProtocolTypes;

        internal ServerRecordEndPoint(string host, ushort port, ProtocolTypes protocolTypes) {
            if (string.IsNullOrEmpty(host) || (port == 0) || (protocolTypes == 0)) {
                throw new ArgumentNullException(nameof(host) + " || " + nameof(port) + " || " + nameof(protocolTypes));
            }
            Host = host;
            Port = port;
            ProtocolTypes = protocolTypes;
        }

        private ServerRecordEndPoint() { }

        public override bool Equals(object obj) {
            if (obj == null) return false;
            if (obj == this) return true;
            return obj is ServerRecordEndPoint serverRecord && Equals(serverRecord);
        }

        public override int GetHashCode() => (Host, Port, ProtocolTypes).GetHashCode();
        private bool Equals(ServerRecordEndPoint other) => string.Equals(Host, other.Host) && (Port == other.Port) && (ProtocolTypes == other.ProtocolTypes);
    }
}
