﻿using SteamKit2.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteamFarm.Server {
    public class InMemoryServerListProvider : IServerListProvider {

        private readonly ConcurrentHashSet<ServerRecordEndPoint> ServerRecords = new ConcurrentHashSet<ServerRecordEndPoint>();
        public Task<IEnumerable<ServerRecord>> FetchServerListAsync() => Task.FromResult(ServerRecords.Select(server => ServerRecord.CreateServer(server.Host, server.Port, server.ProtocolTypes)));

        public Task UpdateServerListAsync(IEnumerable<ServerRecord> endpoints) {
            if (Value.IsNull(endpoints)) return Task.CompletedTask;

            HashSet<ServerRecordEndPoint> newServerRecords = new HashSet<ServerRecordEndPoint>(endpoints.Select(ep => new ServerRecordEndPoint(ep.GetHost(), (ushort)ep.GetPort(), ep.ProtocolTypes)));

            if (!ServerRecords.ReplaceIfNeededWith(newServerRecords)) {
                return Task.CompletedTask;
            }

            ServerListUpdated?.Invoke(this, EventArgs.Empty);
            return Task.CompletedTask;
        }

        internal event EventHandler ServerListUpdated;
    }
}
