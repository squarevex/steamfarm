﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SteamFarm {
    public class BotAuth {

        private Bot Bot;
        private Logger Log;
        private BotWeb Web;

        private static int? SteamTimeDifference;
        private static DateTime LastSteamTimeCheck;

        private static readonly SemaphoreSlim TimeSemaphore = new SemaphoreSlim(1, 1);

        private bool HasCorrectDeviceID => !string.IsNullOrEmpty(DeviceID) && !DeviceID.Equals("ERROR");

        public string DeviceID;
        public string SharedSecret;
        public string IdentitySecret;

        public BotAuth(Bot Bot) {
            this.Bot = Bot;
            this.Log = Bot.Log;
            this.Web = Bot.Web;

            this.DeviceID = Bot.Table.Auth_DeviceID;
            this.SharedSecret = Bot.Table.Auth_SharedSecret;
            this.IdentitySecret = Bot.Table.Auth_IdentitySecret;
        }

        public async Task ImportAuthenticator() {
            if (Bot.Has_Mobile_Authenticator || File.Exists(SharedInfo.AuthPath + Bot.Username + ".maFile")) return;

            Log.Info(Strings.BotAuthenticatorConverting);

            if(!HasCorrectDeviceID) {
                Log.Warning(Strings.BotAuthenticatorInvalidDeviceID);
                if (string.IsNullOrEmpty(DeviceID)) {
                    string deviceID = Input.GetUserInput(Input.EUserInputType.DeviceID, Bot.Username);
                    if (string.IsNullOrEmpty(deviceID)) {
                        Bot.Has_Mobile_Authenticator = false;
                        Bot.Auth.DeviceID = "";
                        Bot.Auth.SharedSecret = "";
                        Bot.Auth.IdentitySecret = "";
                        Bot.Save();
                    }
                }
                CorrectDeviceID(DeviceID);
            }
            Log.Info(Strings.BotAuthenticatorImportFinished);
        }

        public bool CorrectDeviceID(string deviceID) {
            if (string.IsNullOrEmpty(deviceID)) {
                Log.NullError(nameof(deviceID));
                return false;
            }
            if (!string.IsNullOrEmpty(DeviceID) && DeviceID.Equals(deviceID)) return false;
            DeviceID = deviceID;
            return true;
        }

        public async Task<string> GenerateToken() {
            uint time = await GetSteamTime().ConfigureAwait(false);

            if (time == 0) {
                Log.NullError(nameof(time));
                return null;
            }
            return GenerateTokenForTime(time);
        }

        public async Task<uint> GetSteamTime() {
            if (SteamTimeDifference.HasValue && (DateTime.UtcNow.Subtract(LastSteamTimeCheck).TotalHours < SharedInfo.SteamTimeTTL)) {
                return (uint)(Utilities.GetUnixTime() + SteamTimeDifference.Value);
            }
            await TimeSemaphore.WaitAsync().ConfigureAwait(false);
            try {
                if (SteamTimeDifference.HasValue && (DateTime.UtcNow.Subtract(LastSteamTimeCheck).TotalHours < SharedInfo.SteamTimeTTL)) {
                    return (uint)(Utilities.GetUnixTime() + SteamTimeDifference.Value);
                }

                uint serverTime = await Web.GetServerTime().ConfigureAwait(false);
                if (serverTime == 0) return Utilities.GetUnixTime();
                SteamTimeDifference = (int)(serverTime - Utilities.GetUnixTime());
                LastSteamTimeCheck = DateTime.UtcNow;
                return (uint)(Utilities.GetUnixTime() + SteamTimeDifference.Value);
            } finally {
                TimeSemaphore.Release();
            }

        }

        private string GenerateTokenForTime(uint time) {
            if (time == 0) {
                Log.NullError(nameof(time));
                return null;
            }

            byte[] sharedSecret;
            try {
                sharedSecret = Convert.FromBase64String(SharedSecret); // ERROR
            } catch (FormatException e) {
                Log.Exception(e);
                Log.Error(string.Format(Strings.ErrorInvalid, nameof(SharedSecret)));
                return null;
            }
            byte[] timeArray = BitConverter.GetBytes((long)time / SharedInfo.CodeInterval);
            if (BitConverter.IsLittleEndian) Array.Reverse(timeArray);

            byte[] hash;
            using (HMACSHA1 hmac = new HMACSHA1(sharedSecret)) {
                hash = hmac.ComputeHash(timeArray);
            }

            // The last 4 bits of the mac say where the code starts
            int start = hash[hash.Length - 1] & 0x0f;

            // Extract those 4 bytes
            byte[] bytes = new byte[4];
            Array.Copy(hash, start, bytes, 0, 4);
            if (BitConverter.IsLittleEndian) Array.Reverse(bytes);
            uint fullCode = BitConverter.ToUInt32(bytes, 0) & 0x7fffffff;

            // Build the alphanumeric code
            StringBuilder code = new StringBuilder();

            for (byte i = 0; i < SharedInfo.CodeDigits; i++) {
                code.Append(SharedInfo.CodeCharacters[fullCode % SharedInfo.CodeCharacters.Length]);
                fullCode /= (uint)SharedInfo.CodeCharacters.Length;
            }

            return code.ToString();
        }
    }

    public class Confirmation {
        private readonly ulong ID;
        private readonly ulong Key;

        public Confirmation(ulong id, ulong key) {
            if ((id == 0) || (key == 0)) throw new ArgumentNullException(nameof(id) + " || " + nameof(key));
            ID = id;
            Key = key;
        }
    }
}
