﻿using SteamKit2;
using SteamKit2.Discovery;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SteamFarm {
    public class Bot : BotData {

        public  static readonly ConcurrentDictionary<string, Bot> Bots = new ConcurrentDictionary<string, Bot>();
        public static readonly SemaphoreSlim BotsSemaphore = new SemaphoreSlim(1, 1);
        private static readonly SemaphoreSlim LoginSemaphore = new SemaphoreSlim(1, 1);
        private readonly SemaphoreSlim  CallbackSemaphore = new SemaphoreSlim(1, 1);

        public static async Task Register(Table.Bot Table) {
            if (String.IsNullOrEmpty(Table.Username)) return;
            if (Bots.ContainsKey(Table.Username)) return;

            await BotsSemaphore.WaitAsync().ConfigureAwait(false);
            if (Bots.ContainsKey(Table.Username)) return;
            Bot bot = new Bot(Table);
            
        }

        public override void Dispose() {//TODO: NOT DONE
            CallbackSemaphore.Dispose();

            Web?.Dispose();
            ConnectionFailureTimer?.Dispose();
            HeartBeatTimer?.Dispose();
            PlayingWasBlockedTimer?.Dispose();
  
        }

        public Bot(Table.Bot Table) : base(Table) {
            if (String.IsNullOrEmpty(Table.Username)) return;

            if (!Bots.TryAdd(Table.Username, this)) {
                Log.Info(string.Format(Strings.ErrorInvalid, nameof(Table.Username)));
                return;
            }
            this.Log     = new Logger(Table.Username, Table.Username);
            this.Web     = new BotWeb(this);
            this.Event   = new BotEvent(this);
            this.Auth    = new BotAuth(this);
            this.Handler = new BotHandler(this);

            InitStart();

            SteamClient = new SteamClient(SteamConfiguration);
            SteamClient.AddHandler(this.Handler);

            CallbackManager = new CallbackManager(SteamClient);
            CallbackManager.Subscribe<SteamClient.ConnectedCallback>(Event.OnConnected);
            CallbackManager.Subscribe<SteamClient.DisconnectedCallback>(Event.OnDisconnected);

            SteamUser = SteamClient.GetHandler<SteamUser>();
            CallbackManager.Subscribe<SteamUser.LoggedOffCallback>(Event.OnLoggedOff);
            CallbackManager.Subscribe<SteamUser.LoggedOnCallback>(Event.OnLoggedOn);
            CallbackManager.Subscribe<SteamUser.LoginKeyCallback>(Event.OnLoginKey);
            CallbackManager.Subscribe<SteamUser.UpdateMachineAuthCallback>(Event.OnMachineAuth);

            SteamFriends = SteamClient.GetHandler<SteamFriends>();
            CallbackManager.Subscribe<SteamFriends.PersonaStateCallback>(Event.OnPersonaState);

            HeartBeatTimer = new Timer(
                async e => await HeartBeat().ConfigureAwait(false),
                null,
                TimeSpan.FromMinutes(1) + TimeSpan.FromSeconds(SharedInfo.LoginLimiterDelay * Bots.Count), // Delay
                TimeSpan.FromMinutes(1) // Period
            );

        }

        private async Task HeartBeat() {
            if (!KeepRunning || !IsConnectedAndLoggedOn || (HeartBeatFailures == byte.MaxValue)) return;
            try {
                if (DateTime.UtcNow.Subtract(Handler.LastPacketReceived).TotalSeconds > SharedInfo.MinHeartBeatTTL) {
                    #pragma warning disable ConfigureAwaitChecker // CAC001
                    await SteamFriends.RequestProfileInfo(SteamClient.SteamID);
                    #pragma warning restore ConfigureAwaitChecker // CAC001
                }
                HeartBeatFailures = 0;
            } catch (Exception e) {
                Log.Exception(e);
                if (!KeepRunning || !IsConnectedAndLoggedOn || (HeartBeatFailures == byte.MaxValue)) return;

                if (++HeartBeatFailures >= (byte)Math.Ceiling(SharedInfo.ConnectionTimeout / 10.0)) {
                    HeartBeatFailures = byte.MaxValue;
                    Log.Warning(Strings.BotConnectionLost);
                    Utilities.InBackground(() => Connect(true));
                }
            }
        }

        private void HandleCallbacks() {
            TimeSpan timeSpan = TimeSpan.FromMilliseconds(SharedInfo.CallbackSleep);
            while (KeepRunning || SteamClient.IsConnected) {
                if (!CallbackSemaphore.Wait(0)) {
                    Log.Info("Failed due to error: " + nameof(CallbackSemaphore));
                    return;
                }
                try {
                    CallbackManager.RunWaitAllCallbacks(timeSpan);
                } catch (Exception e) {
                    Log.Exception(e);
                } finally {
                    CallbackSemaphore.Release();
                }
            }
        }

        private static async Task LimitLoginRequestsAsync() {
            await LoginSemaphore.WaitAsync().ConfigureAwait(false);
            Utilities.InBackground(async () => {
                await Task.Delay(10000).ConfigureAwait(false);
                LoginSemaphore.Release();
            });
        }

        public async Task<bool> RefreshSession() {
            if (!IsConnectedAndLoggedOn) return false;
            SteamUser.WebAPIUserNonceCallback callback;
            try {
                #pragma warning disable ConfigureAwaitChecker // CAC001
                callback = await SteamUser.RequestWebAPIUserNonce();
                #pragma warning restore ConfigureAwaitChecker // CAC001
            } catch (Exception e) {
                Log.Exception(e);
                await Connect(true).ConfigureAwait(false);
                return false;
            }

            if (string.IsNullOrEmpty(callback?.Nonce)) {
                await Connect(true).ConfigureAwait(false);
                return false;
            }

            string SteamParentPin = "0"; //TODO: SteamParentPIN
            if (await Web.Init(CachedSteamID, SteamClient.Universe, callback.Nonce, SteamParentPin).ConfigureAwait(false)) {
                return true;
            }
            await Connect(true).ConfigureAwait(false);
            return false;
        }

        public void RequestPersonaStateUpdate() {
            if (!IsConnectedAndLoggedOn) return;
            SteamFriends.RequestFriendInfo(CachedSteamID, EClientPersonaStateFlag.PlayerName | EClientPersonaStateFlag.Presence);
        }
        /* ---------------------------------------------------------------------*/
        /* START
        /* -------------------------------------------------------------------- */
        private void InitStart() {
            if (!Enabled) {
                Log.Info(Strings.BotDisabled);
                BotsSemaphore.Release();
                return;
            }
            if (String.IsNullOrEmpty(Username) && String.IsNullOrEmpty(Password)) {
                Log.Info(Strings.BotNoLogin);
                BotsSemaphore.Release();
                return;
            }
            Utilities.InBackground(Start);
        }
        private async Task Start() {
            if (!KeepRunning) {
                KeepRunning = true;
                Utilities.InBackground(HandleCallbacks, true);
                Log.Info(Strings.BotStarting);
            }

            if(!Has_Mobile_Authenticator && File.Exists(SharedInfo.AuthPath + Username + ".maFile")) {
                await Auth.ImportAuthenticator().ConfigureAwait(false);
            }

            await Connect().ConfigureAwait(false);
        }
        public async Task Connect(bool force = false) {
            if (!force && (!KeepRunning || SteamClient.IsConnected)) return;
            await LimitLoginRequestsAsync().ConfigureAwait(false);
            if (!force && (!KeepRunning || SteamClient.IsConnected)) return;

            Log.Info(Strings.BotConnecting);
            InitConnectionFailureTimer();
            SteamClient.Connect();
        }
        public void InitConnectionFailureTimer() {
            if (ConnectionFailureTimer != null) return;
            ConnectionFailureTimer = new Timer(
                async e => await InitPermanentConnectionFailure().ConfigureAwait(false),
                null,
                TimeSpan.FromMinutes(Math.Ceiling(60 / 30.0)), // Delay
                Timeout.InfiniteTimeSpan // Period
            );
        }
        public async Task InitPermanentConnectionFailure() {
            if (!KeepRunning) return;
            Log.Info(Strings.BotHeartBeatFailed);
            Destroy(true);
            await Bot.Register(Table).ConfigureAwait(false);
        }
        public void Destroy(bool force = false) {
            if (!force) Stop();
            else Utilities.InBackground(() => Stop());
            Bots.TryRemove(Username, out _);
        }
        /* ---------------------------------------------------------------------*/
        /* STOP
        /* -------------------------------------------------------------------- */
        public void Stop(bool SkipShutdownEvent = false) {
            if (!KeepRunning) return;
            Log.Info(Strings.BotStopping);
            KeepRunning = false;
            if (SteamClient.IsConnected) Disconnect();
            if (!SkipShutdownEvent) Utilities.InBackground(BotEvent.OnBotShutdown);
        }
        public void Disconnect() {
            StopConnectionFailureTimer();
            SteamClient.Disconnect();
        }
        public void StopConnectionFailureTimer() {
            if (ConnectionFailureTimer == null) return;
            ConnectionFailureTimer.Dispose();
            ConnectionFailureTimer = null;
        }

        /* ---------------------------------------------------------------------*/
        /* PLAYING
        /* -------------------------------------------------------------------- */

        public void ResetPlayingWasBlockedWithTimer() {
            PlayingWasBlocked = false;
            StopPlayingWasBlockedTimer();
        }

        private void StopPlayingWasBlockedTimer() {
            if (PlayingWasBlockedTimer == null) return;
            PlayingWasBlockedTimer.Dispose();
            PlayingWasBlockedTimer = null;
        }
    }
}
