﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using SteamKit2;

namespace SteamFarm {
    public class BotWeb : IDisposable {

        private Bot Bot;
        private Logger Log;

        private readonly WebBrowser WebBrowser;

        private const string IEconService = "IEconService";
        private const string IPlayerService = "IPlayerService";
        private const string ISteamUserAuth = "ISteamUserAuth";
        private const string ITwoFactorService = "ITwoFactorService";
        private const string SteamCommunityHost = "steamcommunity.com";
        private const string SteamCommunityURL = "https://" + SteamCommunityHost;
        private const string SteamStoreHost = "store.steampowered.com";
        private const string SteamStoreURL = "https://" + SteamStoreHost;

        private readonly SemaphoreSlim ApiKeySemaphore = new SemaphoreSlim(1, 1);
        private readonly SemaphoreSlim SessionSemaphore = new SemaphoreSlim(1, 1);

        private string VanityURL;
        private ulong SteamID;
        private DateTime LastSessionRefresh;

        private string CachedApiKey;

        private static readonly Dictionary<string, (SemaphoreSlim RateLimitingSemaphore, SemaphoreSlim OpenConnectionsSemaphore)> WebLimitingSemaphores = new Dictionary<string, (SemaphoreSlim RateLimitingSemaphore, SemaphoreSlim OpenConnectionsSemaphore)>(3) {
            { SteamCommunityURL, (new SemaphoreSlim(1, 1), new SemaphoreSlim(SharedInfo.MaxConnections, SharedInfo.MaxConnections)) },
            { SteamStoreURL, (new SemaphoreSlim(1, 1), new SemaphoreSlim(SharedInfo.MaxConnections, SharedInfo.MaxConnections)) },
            { WebAPI.DefaultBaseAddress.Host, (new SemaphoreSlim(1, 1), new SemaphoreSlim(SharedInfo.MaxConnections, SharedInfo.MaxConnections)) }
        };

        public BotWeb(Bot Bot) {
            this.Bot = Bot;
            this.Log = Bot.Log;

            WebBrowser = new WebBrowser(Log);
        }

        public async Task<bool> Init(ulong steamID, EUniverse universe, string webAPIUserNonce, string parentalPin, string vanityURL = null) {
            if ((steamID == 0) || (universe == EUniverse.Invalid) || string.IsNullOrEmpty(webAPIUserNonce) || string.IsNullOrEmpty(parentalPin)) {
                Log.NullError(nameof(steamID) + " || " + nameof(universe) + " || " + nameof(webAPIUserNonce) + " || " + nameof(parentalPin));
                return false;
            }

            if (!string.IsNullOrEmpty(vanityURL)) VanityURL = vanityURL;

            string sessionID = Convert.ToBase64String(Encoding.UTF8.GetBytes(steamID.ToString()));

            // Generate an AES session key
            byte[] sessionKey = SteamKit2.CryptoHelper.GenerateRandomBlock(32);

            // RSA encrypt it with the public key for the universe we're on
            byte[] cryptedSessionKey;
            using (RSACrypto rsa = new RSACrypto(KeyDictionary.GetPublicKey(universe))) {
                cryptedSessionKey = rsa.Encrypt(sessionKey);
            }

            // Copy our login key
            byte[] loginKey = new byte[webAPIUserNonce.Length];
            Array.Copy(Encoding.ASCII.GetBytes(webAPIUserNonce), loginKey, webAPIUserNonce.Length);

            // AES encrypt the loginkey with our session key
            byte[] cryptedLoginKey = SteamKit2.CryptoHelper.SymmetricEncrypt(loginKey, sessionKey);

            // Do the magic
            Log.Info(string.Format(Strings.WebLogin, ISteamUserAuth));

            KeyValue response = null;
            using (dynamic iSteamUserAuth = WebAPI.GetAsyncInterface(ISteamUserAuth)) {
                iSteamUserAuth.Timeout = WebBrowser.Timeout;

                try {
                    response = await WebLimitRequest(WebAPI.DefaultBaseAddress.Host,
                        #pragma warning disable ConfigureAwaitChecker // CAC001
                        // ReSharper disable once AccessToDisposedClosure
                        async () => await iSteamUserAuth.AuthenticateUser(
                            steamid: steamID,
                            sessionkey: Encoding.ASCII.GetString(WebUtility.UrlEncodeToBytes(cryptedSessionKey, 0, cryptedSessionKey.Length)),
                            encrypted_loginkey: Encoding.ASCII.GetString(WebUtility.UrlEncodeToBytes(cryptedLoginKey, 0, cryptedLoginKey.Length)),
                            method: WebRequestMethods.Http.Post,
                            secure: true
                        )
                        #pragma warning restore ConfigureAwaitChecker // CAC001
                    ).ConfigureAwait(false);
                } catch (TaskCanceledException e) {
                    Log.Exception(e);
                } catch (Exception e) {
                    Log.Exception(e);
                }
            }

            if (response == null) return false;

            string steamLogin = response["token"].Value;
            if (string.IsNullOrEmpty(steamLogin)) {
                Log.NullError(nameof(steamLogin));
                return false;
            }

            string steamLoginSecure = response["tokensecure"].Value;
            if (string.IsNullOrEmpty(steamLoginSecure)) {
                Log.NullError(nameof(steamLoginSecure));
                return false;
            }

            WebBrowser.CookieContainer.Add(new Cookie("sessionid", sessionID, "/", "." + SteamCommunityHost));
            WebBrowser.CookieContainer.Add(new Cookie("sessionid", sessionID, "/", "." + SteamStoreHost));

            WebBrowser.CookieContainer.Add(new Cookie("steamLogin", steamLogin, "/", "." + SteamCommunityHost));
            WebBrowser.CookieContainer.Add(new Cookie("steamLogin", steamLogin, "/", "." + SteamStoreHost));

            WebBrowser.CookieContainer.Add(new Cookie("steamLoginSecure", steamLoginSecure, "/", "." + SteamCommunityHost));
            WebBrowser.CookieContainer.Add(new Cookie("steamLoginSecure", steamLoginSecure, "/", "." + SteamStoreHost));

            Log.Info(Strings.WebLoginSuccess);

            // Unlock Steam Parental if needed
            //if (!parentalPin.Equals("0")) {
            //    if (!await UnlockParentalAccount(parentalPin).ConfigureAwait(false)) {
            //        return false;
            //    }
            //}

            SteamID = steamID;
            LastSessionRefresh = DateTime.UtcNow;
            return true;
        }

        public void Dispose() {
            ApiKeySemaphore.Dispose();
            //PublicInventorySemaphore.Dispose();
            SessionSemaphore.Dispose();
            //TradeTokenSemaphore.Dispose();
            WebBrowser.Dispose();
        }

        public async Task<bool> HasValidApiKey() => !string.IsNullOrEmpty(await GetApiKey().ConfigureAwait(false));

        public async Task<bool> JoinGroup(ulong groupID) {
            if (groupID == 0) return false;
            string request = "/gid/" + groupID;

            // Extra entry for sessionID
            Dictionary<string, string> data = new Dictionary<string, string>(2) { { "action", "join" } };
            return await UrlPostWithSession(SteamCommunityURL, request, data, session: ESession.CamelCase).ConfigureAwait(false);
        }

        private async Task<string> GetApiKey() {
            // if not null, We fetched API key already, and either got valid one, or permanent AccessDenied
            if (CachedApiKey != null) return CachedApiKey;

            // if false, API key is permanently unavailable for limited accounts
            if (Bot.IsAccountLimited) return null;

            // We didn't fetch API key yet
            await ApiKeySemaphore.WaitAsync().ConfigureAwait(false);

            try {
                if (CachedApiKey != null) return CachedApiKey;
                (ESteamApiKeyState State, string Key) result = await GetApiKeyState().ConfigureAwait(false);

                switch (result.State) {
                    case ESteamApiKeyState.AccessDenied:
                        // We succeeded in fetching API key, but it resulted in access denied
                        // Cache the result as empty, API key is unavailable permanently
                        CachedApiKey = string.Empty;
                        break;
                    case ESteamApiKeyState.NotRegisteredYet:
                        // We succeeded in fetching API key, and it resulted in no key registered yet
                        // Let's try to register a new key
                        if (!await RegisterApiKey().ConfigureAwait(false)) {
                            // Request timed out, bad luck, we'll try again later
                            return null;
                        }

                        // We should have the key ready, so let's fetch it again
                        result = await GetApiKeyState().ConfigureAwait(false);
                        if (result.State != ESteamApiKeyState.Registered) {
                            // Something went wrong, bad luck, we'll try again later
                            return null;
                        }

                        goto case ESteamApiKeyState.Registered;
                    case ESteamApiKeyState.Registered:
                        // We succeeded in fetching API key, and it resulted in registered key
                        // Cache the result, this is the API key we want
                        CachedApiKey = result.Key;
                        break;
                    case ESteamApiKeyState.Timeout:
                        // Request timed out, bad luck, we'll try again later
                        return null;
                    default:
                        // We got an unhandled error, this should never happen
                        Log.Error(string.Format(Strings.WarningUnknownValuePleaseReport, nameof(result.State), result.State));
                        return null;
                }
                return CachedApiKey;
            } finally {
                ApiKeySemaphore.Release();
            }
        }

        private async Task<bool> RegisterApiKey() {
            const string request = "/dev/registerkey";

            // Extra entry for sessionID
            Dictionary<string, string> data = new Dictionary<string, string>(4) {
                { "domain", "localhost" },
                { "agreeToTerms", "agreed" },
                { "Submit", "Register" }
            };

            return await UrlPostWithSession(SteamCommunityURL, request, data).ConfigureAwait(false);
        }

        private async Task<(ESteamApiKeyState State, string Key)> GetApiKeyState() {
            const string request = "/dev/apikey?l=english";
            HtmlDocument htmlDocument = await UrlGetToHtmlDocumentWithSession(SteamCommunityURL, request).ConfigureAwait(false);

            HtmlNode titleNode = htmlDocument?.DocumentNode.SelectSingleNode("//div[@id='mainContents']/h2");
            if (titleNode == null) {
                return (ESteamApiKeyState.Timeout, null);
            }

            string title = titleNode.InnerText;
            if (string.IsNullOrEmpty(title)) {
                Log.NullError(nameof(title));
                return (ESteamApiKeyState.Error, null);
            }

            if (title.Contains("Access Denied") || title.Contains("Validated email address required")) {
                return (ESteamApiKeyState.AccessDenied, null);
            }

            HtmlNode htmlNode = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='bodyContents_ex']/p");
            if (htmlNode == null) {
                Log.NullError(nameof(htmlNode));
                return (ESteamApiKeyState.Error, null);
            }

            string text = htmlNode.InnerText;
            if (string.IsNullOrEmpty(text)) {
                Log.NullError(nameof(text));
                return (ESteamApiKeyState.Error, null);
            }

            if (text.Contains("Registering for a Steam Web API Key")) {
                return (ESteamApiKeyState.NotRegisteredYet, null);
            }

            int keyIndex = text.IndexOf("Key: ", StringComparison.Ordinal);
            if (keyIndex < 0) {
                Log.NullError(nameof(keyIndex));
                return (ESteamApiKeyState.Error, null);
            }

            keyIndex += 5;

            if (text.Length <= keyIndex) {
                Log.NullError(nameof(text));
                return (ESteamApiKeyState.Error, null);
            }

            text = text.Substring(keyIndex);
            if ((text.Length != 32) || !Utilities.IsValidHexadecimalString(text)) {
                Log.NullError(nameof(text));
                return (ESteamApiKeyState.Error, null);
            }

            return (ESteamApiKeyState.Registered, text);
        }

        private async Task<bool> UrlPostWithSession(string host, string request, Dictionary<string, string> data = null, string referer = null, ESession session = ESession.Lowercase, byte maxTries = SharedInfo.MaxTries) {
            if (string.IsNullOrEmpty(host) || string.IsNullOrEmpty(request)) {
                Log.NullError(nameof(host) + " || " + nameof(request));
                return false;
            }

            if (maxTries == 0) {
                Log.Warning(string.Format(Strings.ErrorRequestFailedTooManyTimes, SharedInfo.MaxTries));
                return false;
            }

            // If session refresh is already in progress, wait for it
            await SessionSemaphore.WaitAsync().ConfigureAwait(false);
            SessionSemaphore.Release();

            if (SteamID == 0) {
                for (byte i = 0; (i < SharedInfo.ConnectionTimeout) && (SteamID == 0) && Bot.IsConnectedAndLoggedOn; i++) {
                    await Task.Delay(1000).ConfigureAwait(false);
                }

                if (SteamID == 0) {
                    Log.Warning("Failed!"); //TODO: String.WarningFailed
                    return false;
                }
            }

            if (session != ESession.None) {
                string sessionID = WebBrowser.CookieContainer.GetCookieValue(host, "sessionid");

                if (string.IsNullOrEmpty(sessionID)) {
                    Log.NullError(nameof(sessionID));
                    return false;
                }

                string sessionName;

                switch (session) {
                    case ESession.CamelCase:
                        sessionName = "sessionID";
                        break;
                    case ESession.Lowercase:
                        sessionName = "sessionid";
                        break;
                    default:
                        Log.Error(string.Format(Strings.WarningUnknownValuePleaseReport, nameof(session), session));
                        return false;
                }

                if (data != null) {
                    data[sessionName] = sessionID;
                } else {
                    data = new Dictionary<string, string>(1) { { sessionName, sessionID } };
                }
            }

            WebBrowser.BasicResponse response = await WebLimitRequest(host, async () => await WebBrowser.UrlPost(host + request, data, referer).ConfigureAwait(false)).ConfigureAwait(false);
            if (response == null) return false;

            if (IsSessionExpiredUri(response.FinalUri)) {
                if (await RefreshSession(host).ConfigureAwait(false)) {
                    return await UrlPostWithSession(host, request, data, referer, session, --maxTries).ConfigureAwait(false);
                }
                Log.Warning("Warning!"); //TODO: Strings.WarningFailed
                return false;
            }

            // Under special brain-damaged circumstances, Steam might just return our own profile as a response to the request, for absolutely no reason whatsoever - just try again in this case
            if (await IsProfileUri(response.FinalUri).ConfigureAwait(false)) {
                return await UrlPostWithSession(host, request, data, referer, session, --maxTries).ConfigureAwait(false);
            }
            return true;
        }

        private async Task<HtmlDocument> UrlGetToHtmlDocumentWithSession(string host, string request, byte maxTries = SharedInfo.MaxTries) {
            if (string.IsNullOrEmpty(host) || string.IsNullOrEmpty(request)) {
                Log.NullError(nameof(host) + " || " + nameof(request));
                return null;
            }

            if (maxTries == 0) {
                Log.Warning(string.Format(Strings.ErrorRequestFailedTooManyTimes, SharedInfo.MaxTries));
                return null;
            }

            // If session refresh is already in progress, wait for it
            await SessionSemaphore.WaitAsync().ConfigureAwait(false);
            SessionSemaphore.Release();

            if (SteamID == 0) {
                for (byte i = 0; (i < SharedInfo.ConnectionTimeout) && (SteamID == 0) && Bot.IsConnectedAndLoggedOn; i++) {
                    await Task.Delay(1000).ConfigureAwait(false);
                }

                if (SteamID == 0) {
                    Log.Warning("Warning!"); //TODO: Strings.WarningFailed
                    return null;
                }
            }

            WebBrowser.HtmlDocumentResponse response = await WebLimitRequest(host, async () => await WebBrowser.UrlGetToHtmlDocument(host + request).ConfigureAwait(false)).ConfigureAwait(false);
            if (response == null) {
                return null;
            }

            if (IsSessionExpiredUri(response.FinalUri)) {
                if (await RefreshSession(host).ConfigureAwait(false)) {
                    return await UrlGetToHtmlDocumentWithSession(host, request, --maxTries).ConfigureAwait(false);
                }
                Log.Warning("Warning!"); //TODO: Strings.WarningFailed
                return null;
            }

            // Under special brain-damaged circumstances, Steam might just return our own profile as a response to the request, for absolutely no reason whatsoever - just try again in this case
            if (await IsProfileUri(response.FinalUri).ConfigureAwait(false)) {
                return await UrlGetToHtmlDocumentWithSession(host, request, --maxTries).ConfigureAwait(false);
            }

            return response.Content;
        }

        private static bool IsSessionExpiredUri(Uri uri) {
            if (uri == null) {
                Program.Log.NullError(nameof(uri));
                return false;
            }
            return uri.AbsolutePath.StartsWith("/login", StringComparison.Ordinal) || uri.Host.Equals("lostauth");
        }

        private async Task<bool> IsProfileUri(Uri uri, bool waitForInitialization = true) {
            if (uri == null) {
                Log.NullError(nameof(uri));
                return false;
            }

            string profileURL = await GetAbsoluteProfileURL(waitForInitialization).ConfigureAwait(false);
            if (string.IsNullOrEmpty(profileURL)) {
                Log.Warning("Warning!"); //TODO: Strings.WarningFailed
                return false;
            }

            return uri.AbsolutePath.Equals(profileURL);
        }

        private async Task<string> GetAbsoluteProfileURL(bool waitForInitialization = true) {
            if (waitForInitialization && (SteamID == 0)) {
                for (byte i = 0; (i < SharedInfo.ConnectionTimeout) && (SteamID == 0) && Bot.IsConnectedAndLoggedOn; i++) {
                    await Task.Delay(1000).ConfigureAwait(false);
                }

                if (SteamID == 0) {
                    Log.Warning("Warning!"); //TODO: Strings.WarningFailed
                    return null;
                }
            }

            return string.IsNullOrEmpty(VanityURL) ? "/profiles/" + SteamID : "/id/" + VanityURL;
        }

        private async Task<bool> RefreshSession(string host) {
            if (string.IsNullOrEmpty(host)) {
                Log.NullError(nameof(host));
                return false;
            }
            if (!Bot.IsConnectedAndLoggedOn) return false;
            DateTime now = DateTime.UtcNow;
            await SessionSemaphore.WaitAsync().ConfigureAwait(false);
            try {
                if (now < LastSessionRefresh) return true;
                if (!Bot.IsConnectedAndLoggedOn) return false;

                Log.Info(Strings.WebRefreshSession);
                bool result = await Bot.RefreshSession().ConfigureAwait(false);
                if (result) LastSessionRefresh = DateTime.UtcNow;
                return result;
            } finally {
                SessionSemaphore.Release();
            }
        }

        public async Task<uint> GetServerTime() {
            KeyValue response = null;

            for (byte i = 0; (i < SharedInfo.MaxTries) && (response == null); i++) {
                using (dynamic iTwoFactorService = WebAPI.GetAsyncInterface(ITwoFactorService)) {
                    iTwoFactorService.Timeout = WebBrowser.Timeout;
                    try {
                        response = await WebLimitRequest(WebAPI.DefaultBaseAddress.Host,
                            #pragma warning disable ConfigureAwaitChecker // CAC001
                            // ReSharper disable once AccessToDisposedClosure
                            async () => await iTwoFactorService.QueryTime(
                                method: WebRequestMethods.Http.Post,
                                secure: true
                            )
                            #pragma warning restore ConfigureAwaitChecker // CAC001
                        ).ConfigureAwait(false);
                    } catch (TaskCanceledException e) {
                        Log.Exception(e);
                    } catch (Exception e) {
                        Log.Exception(e);
                    }
                }
            }

            if (response == null) {
                Log.Warning(string.Format(Strings.ErrorRequestFailedTooManyTimes, SharedInfo.MaxTries));
                return 0;
            }

            uint result = response["server_time"].AsUnsignedInteger();
            if (result == 0) {
                Log.NullError(nameof(result));
                return 0;
            }

            return result;
        }

        private static async Task<T> WebLimitRequest<T>(string service, Func<Task<T>> function) {
            if (string.IsNullOrEmpty(service) || (function == null)) {
                Program.Log.NullError(nameof(service) + " || " + nameof(function));
                return default;
            }

            if (!WebLimitingSemaphores.TryGetValue(service, out (SemaphoreSlim RateLimitingSemaphore, SemaphoreSlim OpenConnectionsSemaphore) limiters)) {
                Program.Log.Error(string.Format(Strings.WarningUnknownValuePleaseReport, nameof(service), service));
                return await function().ConfigureAwait(false);
            }

            // Sending a request opens a new connection
            await limiters.OpenConnectionsSemaphore.WaitAsync().ConfigureAwait(false);

            try {
                // It also increases number of requests
                await limiters.RateLimitingSemaphore.WaitAsync().ConfigureAwait(false);

                // We release rate-limiter semaphore regardless of our task completion, since we use that one only to guarantee rate-limiting of their creation
                Utilities.InBackground(async () => {
                    await Task.Delay(SharedInfo.WebLimiterDelay).ConfigureAwait(false);
                    limiters.RateLimitingSemaphore.Release();
                });

                return await function().ConfigureAwait(false);
            } finally {
                // We release open connections semaphore only once we're indeed done sending a particular request
                limiters.OpenConnectionsSemaphore.Release();
            }
        }

        //TODO: NOT DONE
        public void OnDisconnected() {
            CachedApiKey = null; //CachedApiKey = CachedTradeToken = null;
            //CachedPublicInventory = null;
            SteamID = 0;
        }

        private enum ESession : byte {
            None,
            Lowercase,
            CamelCase
        }

        private enum ESteamApiKeyState : byte {
            Error,
            Timeout,
            Registered,
            NotRegisteredYet,
            AccessDenied
        }
    }
}
