﻿using SteamFarm.Mysql;
using SteamKit2;
using SteamKit2.Discovery;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SteamFarm {
    public abstract class BotData : IDisposable {

        public Table.Bot  Table;
        public BotHandler Handler;
        public Logger     Log;
        public BotEvent   Event;
        public BotAuth    Auth;
        public BotWeb     Web;

        // TABLE START \\
        public int    Id;
        public bool   Enabled;
        public bool   IsBot;
        public bool   Farm_Offline;
        public string Displayname;
        public string Username;
        public string Password;
        public string Trade_Token;
        public bool   Dismiss_Inventory_Notification;
        public string LoginKey;
        public bool   Has_Mobile_Authenticator;

        // TABLE END \\

        // STEAMKIT START \\
        public CallbackManager  CallbackManager;
        public SteamClient      SteamClient;
        public SteamApps        SteamApps;
        public SteamFriends     SteamFriends;
        public SteamUser        SteamUser;
        // STEAMKIT END \\

        public string AuthCode;
        public string TwoFactorCode;
        public byte TwoFactorCodeFailures;

        public EAccountFlags AccountFlags;

        public bool IsAccountLocked   => AccountFlags.HasFlag(EAccountFlags.Lockdown);
        public bool IsAccountLimited  => AccountFlags.HasFlag(EAccountFlags.LimitedUser) || AccountFlags.HasFlag(EAccountFlags.LimitedUserForce);
        public bool IsPlayingPossible => !PlayingBlocked && (LibraryLockedBySteamID == 0);
        public bool IsConnectedAndLoggedOn => SteamID != 0;
        public bool CanReceiveSteamCards   => !IsAccountLimited && !IsAccountLocked;

        public ulong CachedSteamID;
        public ulong SteamID => SteamClient?.SteamID ?? 0;

        public static SteamConfiguration SteamConfiguration;

        public bool KeepRunning;

        public byte HeartBeatFailures;
        public Timer HeartBeatTimer;
        public Timer ConnectionFailureTimer;

        public EResult  LastLogOnResult;
        public DateTime LastLogonSessionReplaced;

        public bool ReconnectOnUserInitiated;

        public bool PlayingWasBlocked;
        public Timer PlayingWasBlockedTimer;
        public bool  PlayingBlocked;

        public ulong LibraryLockedBySteamID;

        public BotData(Table.Bot Data) {
            this.Table = Data;
            this.Id = Data.Id;
            this.Enabled = Data.Enabled;
            this.IsBot = Data.IsBot;
            this.Farm_Offline = Data.Farm_Offline;
            this.Displayname = Data.Displayname;
            this.Username = Data.Username;
            this.Password = Data.Password;
            this.Trade_Token = Data.Trade_Token;
            this.Dismiss_Inventory_Notification = Data.Dismiss_Inventory_Notification;
            this.LoginKey = Data.LoginKey;
        }

        public static async Task InitializeSteamConfiguration(ProtocolTypes protocolTypes, uint cellID, IServerListProvider serverListProvider) {
            if(Value.IsNull(serverListProvider)) return;

            SteamConfiguration = SteamConfiguration.Create(builder => builder.WithProtocolTypes(protocolTypes).WithCellID(cellID).WithServerListProvider(serverListProvider));
            // Ensure that we ask for a list of servers if we don't have any saved servers available
            IEnumerable<ServerRecord> servers = await SteamConfiguration.ServerListProvider.FetchServerListAsync().ConfigureAwait(false);
            if (servers?.Any() != true) {
                Program.Log.Info(string.Format(Strings.Initializing, nameof(SteamDirectory)));
                try {
                    await SteamDirectory.LoadAsync(SteamConfiguration).ConfigureAwait(false);
                    Program.Log.Info(Strings.InitializingSuccess);
                } catch {
                    Program.Log.Warning(Strings.BotSteamDirectoryInitializationFailed);
                }
            }
        }

        public abstract void Dispose();

        public async void Save() {
            using (var a = new Database()) {
                var T = new Table.Bot {
                    LoginKey = LoginKey,
                    Has_Mobile_Authenticator = Has_Mobile_Authenticator,
                    Auth_DeviceID = Auth.DeviceID,
                    Auth_IdentitySecret = Auth.IdentitySecret,
                    Auth_SharedSecret = Auth.SharedSecret
                };
                //a.Update(T);
                await a.SaveChangesAsync().ConfigureAwait(false);
            }
        }
    }
}
