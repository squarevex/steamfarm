﻿using SteamKit2;
using SteamKit2.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace SteamFarm {
    public class BotHandler : ClientMsgHandler {

        private Bot Bot;
        private Logger Log;

        public DateTime LastPacketReceived;

        public BotHandler(Bot Bot) {
            this.Bot = Bot;
            this.Log = Bot.Log;
        }

        public override void HandleMsg(IPacketMsg packetMsg) {//TODO: NOT DONE
            if (packetMsg == null) {
                Log.NullError(nameof(packetMsg));
                return;
            }
            LastPacketReceived = DateTime.UtcNow;

            switch (packetMsg.MsgType) {
                case EMsg.ClientFSOfflineMessageNotification:
                    //HandleFSOfflineMessageNotification(packetMsg);
                    break;
                case EMsg.ClientItemAnnouncements:
                    //HandleItemAnnouncements(packetMsg);
                    break;
                case EMsg.ClientPlayingSessionState:
                    //HandlePlayingSessionState(packetMsg);
                    break;
                case EMsg.ClientPurchaseResponse:
                    //HandlePurchaseResponse(packetMsg);
                    break;
                case EMsg.ClientRedeemGuestPassResponse:
                    //HandleRedeemGuestPassResponse(packetMsg);
                    break;
                case EMsg.ClientSharedLibraryLockStatus:
                    //HandleSharedLibraryLockStatus(packetMsg);
                    break;
                case EMsg.ClientUserNotifications:
                    //HandleUserNotifications(packetMsg);
                    break;
            }
        }

    }
}
