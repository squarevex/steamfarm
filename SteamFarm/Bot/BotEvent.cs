﻿using SteamFarm.Utils;
using SteamKit2;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using static SteamFarm.CryptoHelper;

namespace SteamFarm {
    public class BotEvent {

        private Bot Bot;
        private Logger Log;
        private BotAuth Auth;
        private BotWeb Web;

        public BotEvent(Bot Bot) {
            this.Bot = Bot;
            this.Log = Bot.Log;
            this.Auth = Bot.Auth;
            this.Web = Bot.Web;
        }

        public static async Task OnBotShutdown() {
            if (Bot.Bots.Values.Any(Bot => Bot.KeepRunning)) return;

            Program.Log.Info(Strings.NoBotsRunning);
            await Task.Delay(5000).ConfigureAwait(false);

            if (Bot.Bots.Values.Any(Bot => Bot.KeepRunning)) return;
            await Program.Exit().ConfigureAwait(false);
        }

        public async void OnConnected(SteamClient.ConnectedCallback callback) {
            if (callback == null) {
                Log.NullError(nameof(callback));
                return;
            }

            Bot.HeartBeatFailures = 0;
            Bot.ReconnectOnUserInitiated = false;
            Bot.StopConnectionFailureTimer();

            Log.Info(Strings.BotConnected);

            if (!Bot.KeepRunning) {
                Log.Info(Strings.BotDisconnecting);
                Bot.Disconnect();
                return;
            }

            byte[] SentryFileHash = null;
            string Path = SharedInfo.SentryPath + Bot.Username + ".bin";
            if (File.Exists(Path)) {
                try {
                    byte[] SentryFileContent = await File.ReadAllBytesAsync(Path).ConfigureAwait(false);
                    SentryFileHash = SteamKit2.CryptoHelper.SHAHash(SentryFileContent);
                } catch (Exception e) {
                    Log.Exception(e);
                    try {
                        File.Delete(Path);
                    } catch {}
                }
            }

            if(!string.IsNullOrEmpty(Bot.LoginKey) && (Bot.LoginKey.Length > 19)) {
                Bot.LoginKey = CryptoHelper.Decrypt(ECryptoMethod.AES, Bot.LoginKey);
            }

            Log.Info(Strings.BotLoggingIn);

            if (string.IsNullOrEmpty(Bot.TwoFactorCode) && Bot.Has_Mobile_Authenticator) {
                Bot.TwoFactorCode = await Bot.Auth.GenerateToken().ConfigureAwait(false);
            }

            Bot.InitConnectionFailureTimer();

            Bot.SteamUser.LogOn(new SteamUser.LogOnDetails {
                AuthCode = Bot.AuthCode,
                CellID = Config.i().CellID,
                LoginID = 1243, //IPC PORT
                LoginKey = Bot.LoginKey,
                Password = Bot.Password,
                SentryFileHash = SentryFileHash,
                ShouldRememberPassword = true,
                TwoFactorCode = Bot.TwoFactorCode,
                Username = Bot.Username
            });

        }

        public async void OnDisconnected(SteamClient.DisconnectedCallback callback) {
            if (callback == null) {
                Log.NullError(nameof(callback));
                return;
            }

            EResult lastLogOnResult = Bot.LastLogOnResult;
            Bot.LastLogOnResult = EResult.Invalid;
            Bot.HeartBeatFailures = 0; //Bot.ItemsCount = Bot.TradesCount = Bot.HeartBeatFailures = 0;
            Bot.StopConnectionFailureTimer();
            //StopPlayingWasBlockedTimer();

            Log.Info(Strings.BotDisconnected);

            Bot.Web.OnDisconnected();
            //CardsFarmer.OnDisconnected();
            //Trading.OnDisconnected();

            //FirstTradeSent = false;
            //HandledGifts.Clear();

            // If we initiated disconnect, do not attempt to reconnect
            if (callback.UserInitiated && !Bot.ReconnectOnUserInitiated) return;

            switch (lastLogOnResult) {
                case EResult.AccountDisabled: return;
                case EResult.Invalid:
                    Bot.AuthCode = Bot.TwoFactorCode = null;
					break;
                case EResult.InvalidPassword:
                    if (string.IsNullOrEmpty(Bot.LoginKey)) goto case EResult.RateLimitExceeded;
                    Bot.LoginKey = null;
                    Bot.Save();
                    Log.Info(Strings.BotRemovedExpiredLoginKey);
                    break;
                case EResult.NoConnection:
                case EResult.ServiceUnavailable:
                case EResult.Timeout:
                case EResult.TryAnotherCM:
                    await Task.Delay(5000).ConfigureAwait(false);
                    break;
                case EResult.RateLimitExceeded:
                    Log.Info(string.Format(Strings.BotRateLimitExceeded, SharedInfo.LoginCooldownInMinutes));
                    await Task.Delay(SharedInfo.LoginCooldownInMinutes * 60 * 1000).ConfigureAwait(false);
                    break;

            }

            if (!Bot.KeepRunning || Bot.SteamClient.IsConnected) return;
            Log.Info(Strings.BotReconnecting);
            await Bot.Connect().ConfigureAwait(false);
        }

        public void OnLoginKey(SteamUser.LoginKeyCallback callback) {
            if (string.IsNullOrEmpty(callback?.LoginKey)) {
                Log.NullError(nameof(callback) + " || " + nameof(callback.LoginKey));
                return;
            }

            string key = callback.LoginKey;

            if (!string.IsNullOrEmpty(key)) key = CryptoHelper.Encrypt(ECryptoMethod.AES, key);
            Bot.LoginKey = key;
            Bot.Save();
            Bot.SteamUser.AcceptNewLoginKey(callback);
        }

        public void OnMachineAuth(SteamUser.UpdateMachineAuthCallback callback) {
            if (callback == null) {
                Log.NullError(nameof(callback));
                return;
            }

            int fileSize;
            byte[] sentryHash;
            string Path = SharedInfo.SentryPath + Bot.Username + ".bin";
            try {
                using (FileStream fileStream = File.Open(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                    fileStream.Seek(callback.Offset, SeekOrigin.Begin);
                    fileStream.Write(callback.Data, 0, callback.BytesToWrite);
                    fileSize = (int)fileStream.Length;

                    fileStream.Seek(0, SeekOrigin.Begin);
                    using (SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider()) {
                        sentryHash = sha.ComputeHash(fileStream);
                    }
                }
            } catch (Exception e) {
                Log.Exception(e);

                try {
                    File.Delete(Path);
                } catch {
                    // Ignored, we can only try to delete faulted file at best
                }

                return;
            }

            // Inform the steam servers that we're accepting this sentry file
            Bot.SteamUser.SendMachineAuthResponse(new SteamUser.MachineAuthDetails {
                JobID = callback.JobID,
                FileName = callback.FileName,
                BytesWritten = callback.BytesToWrite,
                FileSize = fileSize,
                Offset = callback.Offset,
                Result = EResult.OK,
                LastError = 0,
                OneTimePassword = callback.OneTimePassword,
                SentryFileHash = sentryHash
            });
        }

        public void OnLoggedOff(SteamUser.LoggedOffCallback callback) {
            if (callback == null) {
                Log.NullError(nameof(callback));
                return;
            }

            Bot.LastLogOnResult = callback.Result;
            Log.Info(string.Format(Strings.BotLoggedOff, callback.Result));

            switch (callback.Result) {
                case EResult.LoggedInElsewhere:
                    // This result directly indicates that playing was blocked when we got (forcefully) disconnected
                    Bot.PlayingWasBlocked = true;
                    break;
                case EResult.LogonSessionReplaced:
                    DateTime now = DateTime.UtcNow;

                    if (now.Subtract(Bot.LastLogonSessionReplaced).TotalHours < 1) {
                        Log.Error(Strings.BotLogonSessionReplaced);
                        Bot.Stop();
                        return;
                    }

                    Bot.LastLogonSessionReplaced = now;
                    break;
            }
            Bot.ReconnectOnUserInitiated = true;
            Bot.SteamClient.Disconnect();
        }

        public async void OnLoggedOn(SteamUser.LoggedOnCallback callback) {//TODO: NOT DONE
            if (callback == null) {
                Log.NullError(nameof(callback));
                return;
            }
            
            // Always reset one-time-only access tokens
            Bot.AuthCode = Bot.TwoFactorCode = null;

            // Keep LastLogOnResult for OnDisconnected()
            Bot.LastLogOnResult = callback.Result;

            Bot.HeartBeatFailures = 0;
            Bot.StopConnectionFailureTimer();

            switch (callback.Result) {
                case EResult.AccountDisabled:
                    // Those failures are permanent, we should Stop() the bot if any of those happen
                    Log.Warning(string.Format(Strings.BotUnableToLogin, callback.Result, callback.ExtendedResult));
                    Bot.Stop();
                    break;
                case EResult.AccountLogonDenied:
                    string authCode = Input.GetUserInput(Input.EUserInputType.SteamGuard, Bot.Username);
                    if (string.IsNullOrEmpty(authCode)) {
                        Bot.Stop();
                        break;
                    }
                    Bot.AuthCode = authCode;
                    break;
                case EResult.AccountLoginDeniedNeedTwoFactor:
                    if (!Bot.Has_Mobile_Authenticator) {
                        string twoFactorCode = Input.GetUserInput(Input.EUserInputType.TwoFactorAuthentication, Bot.Username);
                        if (string.IsNullOrEmpty(twoFactorCode)) {
                            Bot.Stop();
                            break;
                        }
                        Bot.TwoFactorCode = twoFactorCode;
                    }
                    break;
                case EResult.OK:
                    Log.Info(Strings.BotLoggedOn);

                    // Old status for these doesn't matter, we'll update them if needed
                    Bot.LibraryLockedBySteamID = Bot.TwoFactorCodeFailures = 0;
                    Bot.PlayingBlocked = false;

                    if (Bot.PlayingWasBlocked && (Bot.PlayingWasBlockedTimer == null)) {
                        Bot.PlayingWasBlockedTimer = new Timer(
                            e => Bot.ResetPlayingWasBlockedWithTimer(),
                            null,
                            TimeSpan.FromSeconds(SharedInfo.MinPlayingBlockedTTL), // Delay
                            Timeout.InfiniteTimeSpan // Period
                        );
                    }

                    Bot.AccountFlags = callback.AccountFlags;
                    Bot.CachedSteamID = callback.ClientSteamID;


                    if (Bot.IsAccountLimited) Log.Warning(Strings.BotAccountLimited);
                    if (Bot.IsAccountLocked)  Log.Warning(Strings.BotAccountLocked);

                    if ((callback.CellID != 0) && (callback.CellID != Config.i().CellID)) {
                        Config.i().CellID = callback.CellID;
                        await Config.i().Save().ConfigureAwait(false);
                    }

                    //TODO: Do we need to import authenticator?

                    //TODO: Do we need to specify steamParentPin

                    string SteamParentPin = "0"; //TODO: SteamParentPIN
                    if (!await Web.Init(callback.ClientSteamID, Bot.SteamClient.Universe, callback.WebAPIUserNonce, SteamParentPin, callback.VanityURL).ConfigureAwait(false)) {
                        if (!await Bot.RefreshSession().ConfigureAwait(false)) break;
                    }

                    // Sometimes Steam won't send us our own PersonaStateCallback, so request it explicitly
                    Bot.RequestPersonaStateUpdate();

                    // This will pre-cache API key for eventual further usage
                    Utilities.InBackground(Web.HasValidApiKey);

                    // Join master group https://steamcommunity.com/groups/cweye/memberslistxml/?xml=1
                    Utilities.InBackground(async () => {
                        await Web.JoinGroup(103582791439615117).ConfigureAwait(false);
                    });

                    //https://steamcommunity.com/id/zekhap/

                    //Bot.SteamFriends.AddFriend(76561198023492953); //TODO: FIX ME maybe?

                    if (!string.IsNullOrEmpty(Bot.Displayname)) {
                        Bot.SteamFriends.SetPersonaName(Bot.Displayname);
                    }

                    if(!Bot.Farm_Offline) {
                        Bot.SteamFriends.SetPersonaState(EPersonaState.Online);
                    }
                    
                    //Load next bot!
                    Bot.BotsSemaphore.Release();
                    break;
                case EResult.InvalidPassword:
                case EResult.NoConnection:
                case EResult.PasswordRequiredToKickSession: // Not sure about this one, it seems to be just generic "try again"? #694
                case EResult.RateLimitExceeded:
                case EResult.ServiceUnavailable:
                case EResult.Timeout:
                case EResult.TryAnotherCM:
                case EResult.TwoFactorCodeMismatch:
                    Log.Warning(string.Format(Strings.BotUnableToLogin, callback.Result, callback.ExtendedResult));
                    if ((callback.Result == EResult.TwoFactorCodeMismatch) && Bot.Has_Mobile_Authenticator) {
                        if (++Bot.TwoFactorCodeFailures >= SharedInfo.MaxTwoFactorCodeFailures) {
                            Bot.TwoFactorCodeFailures = 0;
                            Log.Error(string.Format(Strings.BotInvalidAuthenticatorDuringLogin, SharedInfo.MaxTwoFactorCodeFailures));
                            Bot.Stop();
                        }
                    }
                    break;
                default:
                    goto case EResult.AccountDisabled;
            }
        }

        public void OnPersonaState(SteamFriends.PersonaStateCallback callback) {
            if (callback == null) {
                Log.NullError(nameof(callback));
                return;
            }
        }
    }
}
